import React, { useEffect } from 'react';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import NavbarMenu from './components/layout/NavbarMenu';
import Home from './components/pages/home/Home';
import Credits from './components/pages/credits/Credits';

function App() {

  useEffect( () => {
      const handleAccount = (accounts) => {
        // Handle the new accounts, or lack thereof.
        // "accounts" will always be an array, but it can be empty.
        console.log('account changed');
        if(accounts.length === 0)
            alert('Add an account to Metamask');
      }

      window.ethereum.enable();
      window.ethereum.request({ method: 'eth_requestAccounts' });
    
      console.log('selected address ', window.ethereum.selectedAddress);

      window.ethereum.on('accountsChanged', handleAccount, {once:true});
            
      window.ethereum.on('chainChanged', (chainId) => {
          // Handle the new chain.
          // Correctly handling chain changes can be complicated.
          // We recommend reloading the page unless you have good reason not to.
          window.location.reload();
      });
  },[]);

  return (
    <div className="App">
      <Router>
        <NavbarMenu />
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="/credits" element={<Credits />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;

/*
<Router>
          <NavbarMenu />
          <Routes>
            <Route exact path="/upload" element={<Upload />} />
          </Routes>
        </Router>
*/