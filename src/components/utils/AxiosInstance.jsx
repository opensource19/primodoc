var axios = require('axios');

var axiosInstance = axios.create({
  baseURL: process.env.REACT_APP_IPFS || 'http://localhost:5001/',
});

module.exports = axiosInstance;
