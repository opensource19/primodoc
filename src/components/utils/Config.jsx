var configInstance = {
    ipfsAPI: process.env.REACT_APP_IPFS || 'http://localhost:5001',
    ipfsGW: process.env.REACT_APP_IPFSGW || 'http://localhost:8080/',
    contractAddress: process.env.REACT_APP_CONTRACT || '0x1C7698878ab18BcC57b4819eFFA74EaF9062e508',
};

module.exports = configInstance;
