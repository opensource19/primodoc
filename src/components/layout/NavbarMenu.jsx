import React, { useState } from 'react';
import { 
    Collapse,
    Navbar, 
    NavbarToggler, 
    NavbarBrand,
    Nav, 
    NavItem,
    NavLink} from 'reactstrap';
import {Link} from 'react-router-dom'
//import classes from './Layout.module.scss';
import Logo from '../../logo.svg';


const NavbarMenu = () => {
  const [state, updateState] = useState({isOpen: false});

  const toggle = () => {
    updateState({isOpen: !state.isOpen});
  };
  
    return (
      <div >
        <Navbar className="navbar-purple" expand="md">
          <NavbarBrand className="navbar-brand" tag={Link} to="/"><img src={Logo} alt="PrimoDoc" width="80px" />PrimoDoc</NavbarBrand>
          <NavbarToggler className="navbar-toggle" onClick={toggle} />
          <Collapse isOpen={state.isOpen} navbar>
            <Nav  className="mr-auto" navbar>
              <NavItem><NavLink  tag={Link} to="/">Home</NavLink></NavItem>
              <NavItem><NavLink  tag={Link} to="/credits">Credits</NavLink></NavItem>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }


export default NavbarMenu;
