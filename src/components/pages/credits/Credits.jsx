import React from 'react';
import { Container, Label, Row, Col } from "reactstrap";
import classes from './Credits.module.scss';


const Credits = () => {

    return (
        <div>
            <section className={classes.maincontainer}>
                <Container className={classes.page}>
                    <Label>Thanks to the following companies and projects for contribution</Label>
                    <br /><br />
                    <Row>
                        <Col md={{ size: 10, offset: 1 }}>
                            <p>IPFS <a href="https://ipfs.io">website</a></p>
                            <br />
                            <p>DocuHash <a href="https://github.com/FutureProcessing/DocuHash">Github</a> and <a href="https://docuhash.azurewebsites.net/">demo</a></p>
                            <br />
                            <p>PrimoPositum <a href="https://primopositum.com">website</a></p>
                        </Col>
                    </Row>
                </Container>
            </section>
        </div>
    );
};

export default Credits;