import React, { useState, useEffect } from 'react';
import { Container, Button, Label, Row, Col } from "reactstrap";
import axios from '../../utils/AxiosInstance';
import CryptoJS from 'crypto-js';
import FileBase64 from 'react-file-base64';
import abi from '../../utils/abi.json';
import config from '../../utils/Config';
import { ethers } from 'ethers'
import classes from './Home.module.scss';


const Home = () => {

    const ethereum = window.ethereum;
    const provider = new ethers.providers.Web3Provider(ethereum);
    const contract = new ethers.Contract(config.contractAddress, abi, provider)
    const [hashes, setHashes] = useState([]); // state hashes list
    const [details, setDetails] = useState({ipfs: '', date: 0, enabled: false, name: ''}); // state datails

    useEffect( () => {
        getHashesList();
        //listen for event of new item uploaded
        provider.on({address: config.contractAddress, topics: []},(log, event) => {
            getHashesList();
        });
    },[]);

    const uploadFile = async (file) => {
        //console.log(file);
        let password = await ethereum.request({
            method: 'personal_sign',
            params: [ethereum.selectedAddress,ethereum.selectedAddress]
        })
        const formData = new FormData();
        var encrypted = CryptoJS.AES.encrypt(file.base64, password);
            formData.append(
                'file',
                encrypted
            );
            
            // Request made to the backend api            
            axios.post("api/v0/add?stream-channels=true&progress=false", formData, {
                headers: {
                  'Content-Type': 'multipart/form-data'
                }
                })
                .then((response) => {
                    //console.log(response);
                    let hash = CryptoJS.SHA256(file.base64);
                    setIpfsDetails(hash.toString(),response.data.Hash,file.name,file.type);
                })
                .catch(error => {
                    console.log(error);
                });
    };

    const setIpfsDetails = async (hash, ipfs, name, type) => {
        //0x952106d67B706e30fd8eded31Af6582F9D60FFaF
        let signer = provider.getSigner(ethereum.selectedAddress);
        let newc = contract.connect(signer);
        await newc.add(ipfs, hash, name, type);
    };

    const b64toBlob = (dataURI) => {
        let base64 = dataURI.split(',')
        dataURI = base64[1];
        let byteString = window.atob(dataURI);
        let ab = new ArrayBuffer(byteString.length);
        let ia = new Uint8Array(ab);
        
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        return new Blob([ab]);
    };

    const onDownload = async (id, name) => {
        
        axios({
            url: `${config.ipfsGW}ipfs/${id}`, //your url
            method: 'GET',
            responseType: 'blob', // important
        }).then(async (response) => {
            let reader = new FileReader();
            reader.readAsBinaryString(response.data);
            reader.onload = async (e) => {
                let password = await ethereum.request({
                    method: 'personal_sign',
                    params: [ethereum.selectedAddress,ethereum.selectedAddress]
                })
                console.log(e.target.result);
                let decrypted = CryptoJS.AES.decrypt(e.target.result, password).toString(CryptoJS.enc.Utf8);
                console.log(decrypted);
                const url = window.URL.createObjectURL(b64toBlob(decrypted));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', name); //or any other extension
                document.body.appendChild(link);
                link.click();
            }
        });
    };

    const getHashesList = async () => {
        let hashList = await contract.getAllHashes()
        console.log('data: ', hashList)
        setHashes(hashList);
    };

    const getDetails = async (hash) => {
        console.log(hash);
        let detail = await contract.get(hash);
        console.log(detail);
        setDetails({
            name: detail[2],
            ipfs: detail[1],
            date: parseInt(detail[4])*1000,
            enabled: detail[5]
        });
    };

    return (
        <div>
            <section className={classes.maincontainer}>
                <Container className={classes.page}>
                <Label>Select a file to upload</Label>
                    <Row>
                        <Col md={{ size: 10, offset: 1 }}>
                           <FileBase64
                                multiple={false}
                                onDone={uploadFile} />
                        </Col>
                    </Row>
                    <br />
                    
                    <Row>
                        <Col md={{ size: 10, offset: 1 }} className={classes.topPanel}> 
                        </Col>
                    </Row>
                    <Label>Your file list</Label>   
                    <Row>
                        <Col lg='8'>                            
                            <br />
                            <ul>
                            {hashes.map(function(obj, i)  {
                                return <div key={i}><li className={classes.listElement}>{obj.substring(0,25) + '...'}</li><Button color='info' onClick={() => getDetails(obj)}>Details</Button></div>
                            })}
                            </ul>
                            <br />
                            <Button color='primary' onClick={getHashesList}>Update List</Button>
                        </Col>
                        <Col lg='3' className={classes.rightPanel}>
                            <Label className={classes.title}>File name: </Label><br />
                            <Label>{details.name}</Label><br />
                            <Label className={classes.title}>Ipfs id: </Label><br />
                            <Label>{details.ipfs}</Label><br />
                            <Label className={classes.title}>Date: </Label><br />
                            {details.enabled ?
                            <Label>{new Date(details.date).toISOString().replace(/.\d+Z$/g, "").replace("T", ", ") + ' UTC'}</Label>
                            : <Label></Label>}
                            <br />
                            <Button color={details.enabled ? 'success' : 'secondary'} disabled={!details.enabled} onClick={() => onDownload(details.ipfs,details.name)}>Download</Button>
                        </Col>
                    </Row>
                </Container>
            </section>
        </div>
    );
};

export default Home;