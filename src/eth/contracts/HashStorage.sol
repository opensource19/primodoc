pragma solidity >=0.8.0;

import "./Ownable.sol";

contract HashStorage is Ownable{
    mapping (string => DocInfo) collection;
    string[] hashes;
    struct DocInfo {
        string ipfsHash;
        string fileName;
        string fileType;
        uint256 dateAdded;
        bool exist; 
    }

    event HashAdded(string ipfsHash, string fileHash, uint dateAdded);

    constructor () public {
        owner = msg.sender;
    }

    function add(string memory _ipfsHash, string memory _fileHash, string memory _fileName, string memory _fileType) public onlyOwner {
        require(collection[_fileHash].exist == false, "[E1] This hash already exists in contract.");
        DocInfo memory docInfo = DocInfo(_ipfsHash, _fileName, _fileType, block.timestamp, true);
        collection[_fileHash] = docInfo;
        hashes.push(_fileHash);
        emit HashAdded(_ipfsHash, _fileHash, block.timestamp);
    }

    function get(string memory _fileHash) public view returns (string memory, string memory, string memory, string memory, uint256, bool) {
        return (
            _fileHash, 
            collection[_fileHash].ipfsHash,
            collection[_fileHash].fileName,
            collection[_fileHash].fileType,
            collection[_fileHash].dateAdded,
            collection[_fileHash].exist
        );
    }

    function getAllHashes() public view returns ( string[] memory ) {
        return (
            hashes
        );
    }
}

