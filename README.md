# Getting Started with PrimoDoc

Install ipfs following the instruction from the [website](https://ipfs.io/#install)

Install Metamask as Browser [extension}(https://metamask.io/download.html) 

Deploy the contract `HashStorage.sol` on your favorite blockchain. This can be easily done with [Remix](https://remix.ethereum.org/).
Please, note that you must use the wallet account you will use for PrimoDoc as owner of the contract.

Clone the repository from GitLab.

In the project folder use `npm` to intall all the dependencies

```
npm i
```

Configure your system, editing the files  `Config.jsx` and `AxiosInstance.jsx` from `./components/utils/` or export the 3 variable in `Config.jsx`

Run the system with

```
npm start
```

Open the browser and navigate `http://localhost:3000`

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

## Credits

- IPFS [website](https://ipfs.io)
- DocuHash [Github](https://github.com/FutureProcessing/DocuHash) [demo](https://docuhash.azurewebsites.net/)
- PrimoPositum [website](https://primopositum.com)